import UserContext from "../UserContext";
import { useContext, useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Home = () =>{
	const loyverseAPI = "https://api.loyverse.com/v1.0"
	const { token, setToken } = useContext(UserContext)
	const [ items, setItems ] = useState([]);
	const corsProxy = "https://skc-cors-anywhere.herokuapp.com/"

	console.log(token);

	useEffect(()=>{
		fetch(`${corsProxy}${loyverseAPI}/items`,{
			method:"GET",
			headers:{
				Authorization: `Bearer ${token.access_token}`
			}
		}).then(res=>res.json()).then(data=>{
			setItems(data.items)
		})

	},[])


	console.log(items)

	return(
		<>
			<h1>Hello!</h1>
			<h6>List of all Items</h6>
			<p>{items.length}</p>
			<ul>{items.map((item)=>{
				return(
					<li>{item.item_name}</li>
					)
			})}</ul>
			<Link className="btn-primary" to={"/inventory-level"}>go to inventory level</Link>
		</>
	);
}

export default Home;