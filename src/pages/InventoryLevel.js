import { useEffect, useState, useContext } from "react";

import UserContext from "../UserContext"

const InventoryLevel = () =>{

	const loyverseAPI = "https://api.loyverse.com/v1.0"
	const corsProxy = "https://skc-cors-anywhere.herokuapp.com/"

	const {token} = useContext(UserContext);
	const [inventory, setInventory] = useState([]);

	useEffect(()=>{
		fetch(`${corsProxy}${loyverseAPI}/inventory`,{
			method:"GET",
			headers:{
				Authorization: `Bearer ${token.access_token}`
			}
		}).then(res=>res.json()).then(data=>{
			setInventory(data.inventory_levels)
		})
	},[])

	console.log(inventory)


	return(
		<>
			<h1>Hello!</h1>
			<h6>List of all inventories</h6>
			<ul>{inventory.map((item)=>{
					return(
						<li>{item.in_stock}</li>
						)
				})}</ul>
		</>
	);
}

export default InventoryLevel;