import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import { useEffect, useState } from "react";




const Connect = () => {
  const [loggedIn, setLoggedIn] = useState(false);
  // const [user, setUser] = useState(null);

  const scope = "CUSTOMERS_READ%20EMPLOYEES_READ%20INVENTORY_READ%20ITEMS_READ%20ITEMS_WRITE%20PAYMENT_TYPES_READ%20STORES_READ%20SUPPLIERS_READ%20TAXES_READ%20CUSTOMERS_WRITE%20INVENTORY_WRITE%20SUPPLIERS_WRITE%20RECEIPTS_WRITE%20RECEIPTS_READ"

  return (
      <div className="App text-center container-fluid">
        {!loggedIn ? (
          <>
            <img
              className="mb-4"
              src="https://loyverse.town/uploads/monthly_2019_09/316088548_loyverse-logo-v-1024x1024(1).thumb.png.4054e15bd0b2110e79ef6c7ed2e792ad.png"
              width="150"
              alt="some meaningful txt"
            />
            <h1 className="h3 mb-3 font-weight-normal">My Inventory Monitor</h1>
            <Button
              type="primary"
              className="btn"
              size="lg"
              href={`https://api.loyverse.com/oauth/authorize?client_id=jaqrpTf4wUuoA8VNxg9_&scope=${scope}&response_type=code&redirect_uri=https://loyverse-api-testing.vercel.app/oauth/redirect`}
            >
              Log In to Loyverse
            </Button>
          </>
        ) : (
          <>
            <h1>Welcome!</h1>
            <p>
              This is a simple integration between OAuth2 on GitHub with Node.js
            </p>
          </>
        )}
      </div>
  );
}

export default Connect;