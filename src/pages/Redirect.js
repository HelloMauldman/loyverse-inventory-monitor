import { useEffect, useState, useContext } from "react";
import { Navigate, Link } from "react-router-dom";
import { Button } from "react-bootstrap";
// import Home from "./Home"

import UserContext from "../UserContext"

const Redirect = () =>{

	const { token, setToken } = useContext(UserContext)
	const clientId= "jaqrpTf4wUuoA8VNxg9_"
	const clientSecret = "B2IUNA16Hi6cSfioMBWBjp-Q3WkwqJGa8uLQOh0mjNgme5RpqJnIMw=="

	const corsProxy = "https://skc-cors-anywhere.herokuapp.com/"
	const loyverseToken = "https://api.loyverse.com/oauth/token"

	// const [token, setToken] = useState('');
	const [payLoad, setPayLoad] = useState("");
	const [redirect, setRedirect] = useState(false);

	// const token1 = new URLSearchParams(window.location.search).get(
 //      "code"
 //    );
    const getToken = new URLSearchParams(window.location.search).get("code")
    

	useEffect(() => {
	    // setToken(new URLSearchParams(window.location.search).get("code"))
	    const reqBody = {
				client_id: clientId,
				client_secret: clientSecret,
				redirect_uri: "https://loyverse-inventory-monitor.vercel.app/oauth/redirect",
				code:getToken,
				grant_type:"authorization_code"
			}

		const newReqBody = new URLSearchParams(Object.entries(reqBody)).toString();

		// console.log(new URLSearchParams({
		// 		"client_id": clientId,
		// 		"client_secret": clientSecret,
		// 		"redirect_uri": "https://loyverse-api-testing.vercel.app/oauth/redirect",
		// 		"code":getToken,
		// 		"grant_type":"authorization_code"
		// 	}))

	  	fetch(corsProxy + loyverseToken, {
	    	headers:{"Content-Type":"application/x-www-form-urlencoded"},
	    	method:"POST",
	    	body: new URLSearchParams(reqBody)
	    }).then(res=>res.json()).then(data=>{setPayLoad(data)})
	    // console.log(token)
	    console.log(payLoad)

  }, []);

	setToken(payLoad)
	console.log(payLoad)
	return (
		<>
			<h1 className="text-center">Successfully Connected!</h1>
			<Link className="justify-content-center btn-primary" to="/home">Okay!</Link>
		</>
	);
}

export default Redirect;