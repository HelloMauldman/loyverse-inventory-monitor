import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Home from "./pages/Home"
import Redirect from "./pages/Redirect";
import Connect from "./pages/Connect"
import InventoryLevel from "./pages/InventoryLevel";

import { UserProvider } from "./UserContext"
import "./App.css";


function App() {

  const [token, setToken] = useState();



  return(
    <UserProvider value={{token, setToken}} >
      <Router>
        <Routes>
           <Route path="/" element={<Connect/>}/>
           <Route path="/oauth/redirect" element={<Redirect/>}/>
           <Route path="/home" element={<Home/>}/>
           <Route path="/inventory-level" element={<InventoryLevel/>}/>
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
